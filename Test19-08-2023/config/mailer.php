<?php
use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';

function sendNotifyMail($subject, $bodyContent)
{
    $mail = new PHPMailer(true);
    $to = "";
    $mail->isSMTP();
    $mail->Host = "smtp.gmail.com";
    $mail->SMTPAuth = true;
    $mail->Username = "";
    $mail->Password = "";
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->addAddress($to);
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $bodyContent;

    if ($mail->send()) {
        return true;
    } else {
        return false;
    }

}
