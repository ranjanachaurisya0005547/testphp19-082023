<?php
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'test19-08-2023');


$conn = NULL;

try {
    $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    if (!$conn) {
        throw new Exception;
    }
} catch (Exception $ex) {
    echo "Connection Error" . $ex;
}


?>