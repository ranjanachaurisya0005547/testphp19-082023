<?php
require_once __DIR__ . '/config/connection.php';
require_once __DIR__.'/config/mailer.php';

$fullName = $phoneNumber = $email = $formSubject = $formMessage = "";
$fullNameError = $phoneError = $emailError = $subjectError = $messageError = $successMessage = $errorMessage = "";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['submit-btn']) && !empty($_POST['submit-btn'])) {
        $fullName = isset($_POST['full_name']) ? $_POST['full_name'] : NULL;
        $phoneNumber = isset($_POST['phone_number']) ? $_POST['phone_number'] : NULL;
        $email = isset($_POST['email']) ? $_POST['email'] : NULL;
        $formSubject = isset($_POST['subject']) ? $_POST['subject'] : NULL;
        $formMessage = isset($_POST['message']) ? $_POST['message'] : NULL;

        if (empty($fullName)) {
            $fullNameError = 'Full Name is Required !';
        }
        if (empty($phoneNumber)) {
            $phoneError = "Phone Number is Required !";
        } else {
            if (!preg_match("/^[0-9]{10}$/", $phoneNumber)) {
                $phoneError = "Invalid Phone Number !";
            }
        }
        if (empty($email)) {
            $emailError = "Email is Required !";
        } else {
            if (!preg_match("/^[a-zA-Z0-9._]+@[a-zA-Z0-9.]+\.[a-zA-Z]{2,4}$/", $email)) {
                $emailError = "Invalid Email !";
            }
        }

        if (empty($formSubject)) {
            $subjectError = 'Subject is Required !';
        }

        if (empty($formMessage)) {
            $messageError = 'Message is Required !';
        }

        if (empty($fullNameError) && empty($phoneError) && empty($emailError) && empty($subjectError) && empty($messageError)) {
            $insert_query = "INSERT INTO contact_form (full_name,phone_number,email,subject,message) values('$fullName','$phoneNumber','$email','$formSubject','$formMessage')";
            $result = mysqli_query($conn, $insert_query);
            if ($result) {

               $body='<htm>

               <head>
                   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                       integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                       integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
                   </script>
                   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
                       integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
                   </script>
               </head>
           
               <body>
                   <div class="conatiner">
                       <div class="row">
                           <div class="col-sm-2"></div>
                           <div class="col-sm-8">
                               <div class="card">
                                   <div class="card-body">
                                       <p><b>Client Name:</b>'.$fullName.'</p>
                                       <p><b>Phone:</b>'.$phoneNumber.'</p>
                                       <p><b>Email:</b>'.$email.'</p>
                                       <p style="margin-top:10px;text-align:justify;"></p>'.$formMessage.'</p>
                                   </div>
                               </div>
                           </div>
                           <div class="col-sm-2"></div>
                       </div>
                   </div>
               </body>
           
               </html>';
               if(sendNotifyMail($formSubject,$body)){
                    $successMessage = "Submitted Successfully !";
                    $fullName = $phoneNumber = $email = $formSubject = $formMessage = "";
               }

            } else {
                $errorMessage = "Submission Failed !";
            }
        }
    }
}
?>

<html>

<head>
    <title>Contact Us</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <div class="row mt-3">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <?php if(!empty($successMessage)){?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo $successMessage; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php }else if(!empty($errorMessage)){?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo $errorMessage; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php }?>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Contact Us</div>
                    <div class="card-body">
                        <form method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Full Name<span
                                        class="required text-danger">*</span>:</label>
                                <input type="full_name" name="full_name" class="form-control" id="fullName"
                                    aria-describedby="fullName" placeholder="Enter Your Full Name"
                                    value="<?php echo $fullName; ?>">
                                <span class="text-danger">
                                    <?php echo $fullNameError; ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone Number<span class="required text-danger">*</span>:</label>
                                <input type="text" name="phone_number" class="form-control" id="phone"
                                    placeholder="Enter Your Phone Number" value="<?php echo $phoneNumber; ?>">
                                <span class="text-danger">
                                    <?php echo $phoneError; ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email<span class="required text-danger">*</span>:</label>
                                <input type="email" name="email" class="form-control" id="email"
                                    placeholder="Enter Your Email" value="<?php echo $email; ?>">
                                <span class="text-danger">
                                    <?php echo $emailError; ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject<span class="required text-danger">*</span>:</label>
                                <input type="text" name="subject" class="form-control" id="subject"
                                    placeholder="Enter Your Subject" value="<?php echo $formSubject; ?>">
                                <span class="text-danger">
                                    <?php echo $subjectError; ?>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="message">Message<span class="required text-danger">*</span>:</label>
                                <textarea class="form-control" id="message"
                                    name="message"><?php echo $formMessage; ?></textarea>
                                <span class="text-danger">
                                    <?php echo $messageError; ?>
                                </span>
                            </div>
                            <input type="submit" name="submit-btn" class="btn btn-primary" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</body>

</html>